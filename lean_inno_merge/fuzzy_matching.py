import pandas as pd
import numpy as np
from geopy import distance
from fuzzywuzzy import fuzz


def apply_matching(df_lean, df_innoplexia):
    """Perform matching of LeAn and Innoplexia businesses based on geographical and name-based proximity.
    This algorithm iterates through all rows in the LeAn dataset, pre-selects candidate LeAn matches that are within 50 meters 
    and computes the Levenstein string distance for all candidates. A match is determined if the Levenshtein score for the best
    candidate exceeds 75"""

    def fuzzy_match(row):
        name = row["businessName"]
        lat = row["result_latitude"]
        long = row["result_longitude"]

        df_lean_with_geodata = df_lean.loc[
            (df_lean['geolocationPoint.point.latitude'].isna() == False)
            &
            (df_lean['geolocationPoint.point.longitude'].isna() == False)
            ]
        dist_filter = df_lean_with_geodata.apply(lambda l: distance.distance((l["geolocationPoint.point.latitude"], l["geolocationPoint.point.longitude"]), (lat, long)).m < 50, axis=1)
        candidates = df_lean_with_geodata[(dist_filter) & ~df_lean_with_geodata["propertyUsers.person.name"].isna()].copy()
        
        if candidates.shape[0] > 0:
            print(f"Computing {candidates.shape[0]} candidates")
            candidates["scores"] = candidates.apply(lambda candidate: fuzz.ratio(candidate["propertyUsers.person.name"], name), axis=1)
            if candidates["scores"].max() > 75:
                return candidates["scores"].idxmax()
        return np.nan

    # Collapse to unique businesses 
    df_innoplexia_unique = df_innoplexia.groupby(["businessName", "result_latitude", "result_longitude"]).first().reset_index().copy()
    print(f"Matching {df_innoplexia_unique.shape[0]} innoplexia rows")
    df_innoplexia_unique["match_id"] = df_innoplexia_unique.apply(lambda row: fuzzy_match(row), axis=1)
    print("Matching complete...")
    
    # Join back to non-unique df
    df_innoplexia = df_innoplexia.merge(df_innoplexia_unique[["businessName", "result_latitude", "result_longitude", "match_id"]], on=["businessName", "result_latitude", "result_longitude"], how='left')
    
    # Merge matches and return full df
    return pd.merge(df_innoplexia, df_lean.reset_index(), left_on="match_id", right_on="index", how="outer")


def clean_matched_buildings(local_businesses_df_matched: pd.DataFrame) -> pd.DataFrame:
    column_pairs = {
        # name in innoplexia : name used in mariadb
        'lean_usage_type_name':'propertyUsers.industryClassification.name',
        'lean_usage_type':'propertyUsers.industryClassification.levelOne',
        'lean_usage_level_one':'propertyUsers.industryClassification.levelTwo',
        'lean_usage_level_two':'propertyUsers.industryClassification.levelThree',
        'businessName':'propertyUsers.person.name',
        'result_latitude':'geolocationPoint.point.latitude',
        'result_longitude':'geolocationPoint.point.longitude',
        'polygon_id':'lagepolygon_id',
        'geometry_for_innoplexia':'geometry'
    }
    columns_kept = [
        'google_business_type',
        'building_unit_id',
        'objectIsEmpty',
        'stars',
        'numberOfReviews'
    ]
    local_businesses_matched_cleaned_df = local_businesses_df_matched[columns_kept].copy()
    for inno_col, lean_col in column_pairs.items():
        local_businesses_matched_cleaned_df.loc[:,lean_col] = local_businesses_df_matched[lean_col].combine_first(
            local_businesses_df_matched[inno_col]
        )

    int_columns = [
        'propertyUsers.industryClassification.levelOne',
        'propertyUsers.industryClassification.levelTwo',
        'propertyUsers.industryClassification.levelThree',
    ]
    for col in int_columns:
        local_businesses_matched_cleaned_df[col] = local_businesses_matched_cleaned_df[col].fillna(1).astype(int)
    
    return local_businesses_matched_cleaned_df