# general imports
import pandas as pd
from configparser import ConfigParser
from datetime import datetime as dt
import os
import geopandas as gpd
import logging

# custom imports
from general_helpers import analyse_availability, hash_business
from geo_helpers import get_lagepolygone, create_lean_geodf, get_inner_city_bounds

from data_prep.inno_prep import get_local_businesses, get_innoplexia_cleaned
from db_connect import db_manager
from lean_inno_merge.fuzzy_matching import apply_matching, clean_matched_buildings

from indicator_calculation.city_indicators import get_city_indicators
from indicator_calculation.business_indicators import get_business_indicators
from indicator_calculation.industry_indicators import get_industry_data, get_industry_based_percentiles
from indicator_calculation.polygon_indicators import get_polygon_indicators, add_polygon_indicators
from indicator_calculation.final import create_full_indicator_df, score_pressures, get_classification, add_missing_business_location_areas



# ========
# constant value definition
# ========
general_config = ConfigParser()
general_config.read("general_config.ini")
basic_path = os.path.join(
    general_config['basic_directories']['shared_directory'],
    general_config['basic_directories']['score_attraction_directory'] 
)
input_path = os.path.join(basic_path, general_config['basic_directories']['input_directory'])
intermediate_path = os.path.join(basic_path, general_config['basic_directories']['intermediate_directory'])
output_path = os.path.join(basic_path, general_config['basic_directories']['output_directory'])
city_reference_path = os.path.join(input_path,general_config['reference_files']['city_reference_file_name'])
zscore_percentile_converter_path = os.path.join(input_path,general_config['reference_files']['zscore_percentile_conversion'])
industry_mapping_path = os.path.join(input_path,general_config['reference_files']['industry_google_lean_reference_filename'])
industry_data_path = os.path.join(input_path,general_config['data_files_in_shared_dir']['ifh_industry_data'])
ifh_file_path = os.path.join(input_path,general_config['data_files_in_shared_dir']['ifh_city_data'])


connection_string = os.environ.get('CONNECTIONSTRING')
# connection_string = 


log_msg = f'Connection string read from environment variable.'
logging.info(log_msg)
polygon_table_name = 'business_location_area'
building_unit_table_name = 'building_unit'

db_schema_name = connection_string[connection_string.rfind('/')+1:]
log_msg = f'Schema name extracted: {db_schema_name}'
logging.info(log_msg)


# api tokens
# todo: laden der sources liste sauber gestalten
api_sources = [
    connection[1] for connection in 
    general_config.items('api_connections_with_token')
]

innoplexia_token = db_manager.get_api_token(
    'api_token_table',
    db_schema_name,
    'innoplexia'
)

files_to_be_loaded_dict = dict(general_config.items('data_files_in_shared_dir'))
files_to_be_loaded_dict.update(dict(general_config.items('reference_files')))

stadt = os.environ.get('CITYNAME')
# stadt = general_config['city']['city']

city_indicator_list = [
    indicator[1] for indicator in 
    general_config.items('city_indicators')
]

max_file_age = int(general_config['settings']['age_to_refresh'])
min_businesses_per_polygon = int(general_config['settings']['min_businesses_per_polygon'])

# initiate logging
current_time = dt.now().strftime("%d-%m-%y_%H-%M-%S")
logging.basicConfig(
    filename = os.path.join(basic_path,f'logging_{current_time}.log'),
    level = logging.DEBUG,
    format='%(asctime)s %(message)s',
    force=True #TODO drop force if possible
)
logging.info("==============")
logging.info("new run")
logging.info("==============")


# analyze availability of files
availability_dict = analyse_availability(
    input_path, 
    intermediate_path, 
    files_to_be_loaded_dict
)

# =========
# compute indicators
# =========

# get city level indicators
city_indicators_df = get_city_indicators(
    max_file_age,
    availability_dict,
    city_reference_path,
    zscore_percentile_converter_path,
    city_indicator_list,
    ifh_file_path,
    stadt
)

# get polygon level indicators
lagepolygone_gdf = get_lagepolygone(schema_name = db_schema_name, table_name = polygon_table_name)
lean_df = db_manager.read_table(schema_name = db_schema_name, table_name = building_unit_table_name)
log_msg = f'Imported {len(lean_df)} businesses from LeAn.'
logging.info(log_msg)

# compute hashes for easier duplicate detection
lean_df.loc[:,'lean_hash'] = [
    hash_business(lat, long, name)
    for lat, long, name
    in zip(
        lean_df['geolocationPoint.point.latitude'], 
        lean_df['geolocationPoint.point.longitude'],
        lean_df['propertyUsers.person.name']
        )
]
lean_df = lean_df.drop_duplicates(subset = ['lean_hash'])
log_msg = f'After dropping duplicates based on the hash there are now {len(lean_df)} entries in lean_df.'

lean_gdf = create_lean_geodf(lean_df)
log_msg = f'lean_gdf has {len(lean_gdf)} entries.'
logging.info(log_msg)
# add polygon_id to LeAn units
lean_gdf = gpd.sjoin(lean_gdf,lagepolygone_gdf,how='left', predicate = 'within')
log_msg = f'After performing spatial join, there are now {len(lean_gdf)} entries in lean_gdf.'
logging.info(log_msg)
lean_gdf = lean_gdf.drop_duplicates(subset = ['lean_hash'])
log_msg = f'After dropping duplicates, there are now {len(lean_gdf)} entries in lean_gdf. Duplicates were identified by lean_hash'
logging.info(log_msg)
lean_gdf.to_csv('shared_directory\score_attraction\intermediate\lean_cleaned.csv')

bounds_dict = get_inner_city_bounds(lagepolygone_gdf)

leerstandsquote_percentile_df = get_polygon_indicators(
    lean_gdf,
    availability_dict,
    max_file_age,
    min_businesses_per_polygon
    )


inno_raw_df = get_innoplexia_cleaned(bounds_dict,industry_mapping_path, innoplexia_token)
inno_raw_df.to_csv('shared_directory\score_attraction\intermediate\inno_cleaned.csv')
cols_to_round = ['result_longitude','result_latitude','scan_latitude','scan_longitude']
for col in cols_to_round:
    inno_raw_df[col] = inno_raw_df[col].round(4)

# local businesses 
# from google
local_businesses_df = get_local_businesses(
    inno_raw_df, 
    lagepolygone_gdf,
    availability_dict,
    max_file_age
)

# to keep polygon_id
try:
    lean_df.loc[:,'lagepolygon_id'] = [
        polygon_id for polygon_id in lean_gdf.polygon_id
    ]
except ValueError as e:
    logging.error(e)
#  matching
lean_df_empty = lean_df.query('objectIsEmpty == True')
lean_df_inuse = lean_df.query('objectIsEmpty == False')
log_msg = f'Number of known objects in use: {len(lean_df_inuse)}. Number of known objects empty: {len(lean_df_empty)}.'
logging.info(log_msg)

local_businesses_df_matched = apply_matching(lean_df_inuse, local_businesses_df.rename({'title':'businessName'},axis=1))
log_msg = f'after matching there are {len(local_businesses_df_matched)} businesses'
logging.info(log_msg)
# only for dev
local_businesses_df_matched.to_csv('shared_directory\score_attraction\intermediate\local_businesses_matched.csv')
# local_businesses_df_matched = pd.read_csv('shared_directory\score_attraction\intermediate\local_businesses_matched.csv')
# TODO: refactor the addition of the geometry col
# TODO: clean up the (clean_matched_buildings)-call to account for the refactoring
local_businesses_df_matched.loc[:,'geometry_for_innoplexia'] = gpd.points_from_xy(
    x = local_businesses_df_matched.result_longitude,
    y = local_businesses_df_matched.result_latitude
)
local_businesses_matched_cleaned_df = clean_matched_buildings(local_businesses_df_matched)
log_msg = f'after cleaning the matched data there are {len(local_businesses_matched_cleaned_df)} businesses'
logging.info(log_msg)

lean_indicator_df = local_businesses_matched_cleaned_df.loc[local_businesses_matched_cleaned_df.building_unit_id.isna() == False].copy()
lean_indicator_df = lean_indicator_df.sort_values('building_unit_id').drop_duplicates(subset = ['building_unit_id'])
taod_indicator_df = local_businesses_matched_cleaned_df.loc[local_businesses_matched_cleaned_df.building_unit_id.isna()].copy()

local_businesses_matched_cleaned_df = pd.concat([lean_indicator_df, taod_indicator_df])
local_businesses_matched_cleaned_df.to_csv(r'shared_directory\score_attraction\intermediate\local_businesses_matched_cleaned.csv')
log_msg = f'After dropping duplicates based on building_unit_id there are {len(local_businesses_matched_cleaned_df)} entries remaining.'
logging.info(log_msg)


# industry data
industry_df = get_industry_data(industry_data_path)

industry_df = industry_df.rename({
        'Nutzungsart':'propertyUsers.industryClassification.levelOne',
        'Differenzierungsebene1':'propertyUsers.industryClassification.levelTwo',
        'Differenzierungsebene2':'propertyUsers.industryClassification.levelThree'
    },axis=1)

local_businesses_with_industry_indicators = local_businesses_matched_cleaned_df.merge(
    industry_df,
    how='left',
    on = [
        'propertyUsers.industryClassification.levelOne',
        'propertyUsers.industryClassification.levelTwo',
        'propertyUsers.industryClassification.levelThree'
    ]    
    )

industry_percentile_df = get_industry_based_percentiles(local_businesses_with_industry_indicators)

local_businesses_with_industry_indicators = local_businesses_with_industry_indicators[[
    'google_business_type',
    'geolocationPoint.point.latitude', 'geolocationPoint.point.longitude',
    'propertyUsers.person.name',
    'Marktvolumen', 'branchenentwicklung_longterm', 'branchenentwicklung_shortterm',
    'fachhandelsanteil_aktuell', 'fachhandelsanteil_entwicklung',
    'onlineanteil_aktuell', 'onlineanteil_entwicklung','ratio_stationaer_online'
]].join(industry_percentile_df, rsuffix='__percentile')

local_businesses_leerstandspercentile_df = add_polygon_indicators(local_businesses_matched_cleaned_df, leerstandsquote_percentile_df)

business_indicators_df = get_business_indicators(
    local_businesses_matched_cleaned_df.rename({
        'propertyUsers.person.name':'title',
        'geolocationPoint.point.latitude':'result_latitude',
        'geolocationPoint.point.longitude':'result_longitude'
    }, axis=1),
    inno_raw_df,
    input_path
)

print(leerstandsquote_percentile_df.reset_index().columns)

full_indicator_df = create_full_indicator_df(
    business_indicators_df,
    local_businesses_with_industry_indicators,
    leerstandsquote_percentile_df.reset_index()[[
        'polygon_id', 
        'n_of_building_units', 
        'leerstandsquote',
        'leerstandsquote__percentile']],
    city_indicators_df
)

log_msg = f'after joining all indicators there are {len(full_indicator_df)} businesses'
logging.info(log_msg)
lean_indicator_df = full_indicator_df.loc[full_indicator_df.building_unit_id.isna() == False].copy()
lean_indicator_df = lean_indicator_df.sort_values('building_unit_id').drop_duplicates(subset = ['building_unit_id'])
taod_indicator_df = full_indicator_df.loc[full_indicator_df.building_unit_id.isna()].copy()

# concatenating after applying deduplication
# adding empty objects as well after all indicators were computed 
full_indicator_df = pd.concat([lean_indicator_df, taod_indicator_df, lean_df_empty]).reset_index(drop=True)
log_msg = f'After dropping duplicates based on building_unit_id there are {len(full_indicator_df)} entries remaining.'
logging.info(log_msg)
# ======== all indicators computed (raw values & percentiles/z-scores)
# ======== scoring 

indicator_level_dict = {
    'business_level':[
        'stars_percentile',
        'numberOfReviews_percentile',
        'local_search_position_percentile',
        'potential_reach__households_percentile'
    ],
    'industry_level':[
        'Marktvolumen__percentile',
        'branchenentwicklung_longterm__percentile',
        'branchenentwicklung_shortterm__percentile',
        'fachhandelsanteil_aktuell__percentile',
        'fachhandelsanteil_entwicklung__percentile',
        'onlineanteil_aktuell__percentile',
        'onlineanteil_entwicklung__percentile',
        'ratio_stationaer_online__percentile'
    ],
    'polygon_level':[
        'leerstandsquote__percentile'
    ],
    'city_level':[ 
        'Kaufkraftniveau__percentile',
        'Zentralitaetsniveau__percentile',
        'Gesamtattraktivitaet__percentile',
        'Attraktivitaetsentwicklung__percentile',
        'NPS__percentile',
        'Bevoelkerungswachstum__percentile'
    ]
}

for level, indicator_list in indicator_level_dict.items():
    full_indicator_df.loc[:,level+'_mean'] = full_indicator_df[indicator_list].mean(axis=1)

full_indicator_df = score_pressures(full_indicator_df)
full_indicator_df.loc[
    full_indicator_df.objectIsEmpty == 1, 
    ['business_level_score_reduction'
        ,'industry_level_score_reduction'
        ,'polygon_level_score_reduction'
        ,'city_level_score_reduction'
        ,'pressure_classification']
] = pd.NA

# push to db
local_businesses_matched_cleaned_df.loc[:,'taod_building_unit_id'] = local_businesses_matched_cleaned_df.index
full_indicator_df.loc[:,'taod_building_unit_id'] = full_indicator_df.index

# rename to be compatible with LeAn
full_indicator_df.rename({
    'propertyUsers.industryClassification.name':'propertyUsers_industryClassification_name',
    'propertyUsers.industryClassification.levelOne':'propertyUsers_industryClassification_levelOne',
    'propertyUsers.industryClassification.levelTwo':'propertyUsers_industryClassification_levelTwo',
    'propertyUsers.industryClassification.levelThree':'propertyUsers_industryClassification_levelThree',
    'title':'propertyUsers_person_name',
    'lagepolygon_id':'business_location_area_id'
    # ,'geometry':'geolocation_point'
},axis=1, inplace=True)


# push new building_units
db_manager.create_table(
    df = full_indicator_df,
    schema_name = db_schema_name,
    columns = [
        'building_unit_id',
        'taod_building_unit_id',
        'business_location_area_id',
        'objectIsEmpty',
        'propertyUsers_industryClassification_name',
        'propertyUsers_industryClassification_levelOne',
        'propertyUsers_industryClassification_levelTwo',
        'propertyUsers_industryClassification_levelThree',
        'propertyUsers_person_name',
        'geometry'
    ],
    table_name = 'enriched_building_units',
    primary_key = 'taod_building_unit_id'
)


# rename indicators to fit naming convention in LeAn
full_indicator_df.rename({
   # 'polygon_id':'business_location_area_id',
    'polygon_level_mean':'business_location_area_level_mean',
    'polygon_level_score_reduction':'business_location_area_level_score_reduction'
}, axis=1, inplace=True)


# push indicators + classification
db_manager.create_table(
    df = full_indicator_df,
    schema_name = db_schema_name,
    columns = [
        'building_unit_id',
        'taod_building_unit_id',
        'business_location_area_id',
        'stars',
        'numberOfReviews',
        'local_search_position', 'potential_reach__households',
        'stars_percentile', 'numberOfReviews_percentile',
        'local_search_position_percentile',
        'potential_reach__households_percentile', 'Marktvolumen',
        'branchenentwicklung_longterm', 'branchenentwicklung_shortterm',
        'fachhandelsanteil_aktuell', 'fachhandelsanteil_entwicklung',
        'onlineanteil_aktuell', 'onlineanteil_entwicklung',
        'ratio_stationaer_online', 'Marktvolumen__percentile',
        'branchenentwicklung_longterm__percentile',
        'branchenentwicklung_shortterm__percentile',
        'fachhandelsanteil_aktuell__percentile',
        'fachhandelsanteil_entwicklung__percentile',
        'onlineanteil_aktuell__percentile',
        'onlineanteil_entwicklung__percentile',
        'ratio_stationaer_online__percentile', 'leerstandsquote',
        'leerstandsquote__percentile', 'Kaufkraftniveau',
        'Zentralitaetsniveau', 'Gesamtattraktivitaet',
        'Attraktivitaetsentwicklung', 'NPS', 'Bevoelkerungswachstum',
        'Kaufkraftniveau__percentile', 'Zentralitaetsniveau__percentile',
        'Gesamtattraktivitaet__percentile', 'Attraktivitaetsentwicklung__percentile',
        'NPS__percentile', 'Bevoelkerungswachstum__percentile', 'business_level_mean',
        'industry_level_mean',
        'business_location_area_level_mean', 'city_level_mean',
        'business_level_score_reduction', 'industry_level_score_reduction', 
        'business_location_area_level_score_reduction',
        'city_level_score_reduction', 'pressure_classification'
    ],
    table_name = 'building_units_scoring',
    primary_key='taod_building_unit_id'
)

full_indicator_df.to_excel(r'shared_directory\score_attraction\intermediate\full_indicator.xlsx')

# push polygon scoring
polygon_aggregation_df = pd.DataFrame(full_indicator_df.groupby(['business_location_area_id']).pressure_classification.mean()).reset_index()

polygon_aggregation_df.loc[:,'pressure_classification'] = [
    get_classification((2-ratio)*50)
    for ratio in polygon_aggregation_df.pressure_classification
]

polygon_aggregation_df = add_missing_business_location_areas(
    lagepolygone_gdf,
    polygon_aggregation_df
)

db_manager.create_table(
    df = polygon_aggregation_df,
    columns = [
        'business_location_area_id',
        'pressure_classification'
    ],
    table_name = 'business_location_area_scoring',
    schema_name = db_schema_name,
    primary_key = 'business_location_area_id'
)

city_aggregation_df = full_indicator_df.pressure_classification.mean()
city_aggregation_df = pd.DataFrame(data = {'city_pressure':city_aggregation_df}, index =[0])
db_manager.create_table(
    df = city_aggregation_df.reset_index(),
    schema_name = db_schema_name,
    columns = [
        'index',
        'city_pressure'
    ],
    table_name = 'city_scoring',
    primary_key = 'index'
)

db_manager.alter_tables()