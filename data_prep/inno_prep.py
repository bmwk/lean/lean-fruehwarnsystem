import pandas as pd
import geopandas as gpd
import logging
import os
import requests


def compute_search_result_distance(
    search_lat_series: pd.Series,
    search_lon_series: pd.Series,
    result_lat_series: pd.Series,
    result_lon_series: pd.Series,
    crs = 'epsg:4839'
    ) -> pd.Series:
    search_gdf = gpd.GeoDataFrame(geometry = gpd.points_from_xy(search_lon_series, search_lat_series), crs = crs)
    result_gdf = gpd.GeoDataFrame(geometry = gpd.points_from_xy(result_lon_series, result_lat_series), crs = crs)
    distance_series = search_gdf.distance(result_gdf)
    return distance_series


def get_available_geomarkets(
    token:str
    ):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-API-Key':token
    }
    geomarket_url = 'https://data.innoplexia.com/lean/geomarkets'
    res = requests.get(geomarket_url, headers=headers)
    cols = res.json()['header']
    data = res.json()['rows']
    return pd.DataFrame(data, columns =cols)

def get_geomarket_data(
    geomarket_ids,
    geomarkets_df: pd.DataFrame,
    token: str
    ):
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-API-Key':token
    }
    list_of_dfs = []
    for id in geomarket_ids:
        data_url = f'https://data.innoplexia.com/lean/geomarket-data/{id}'
        res = requests.get(data_url, headers=headers)
        cols = res.json()['header']
        data = res.json()['rows']
        new_df = pd.DataFrame(data, columns =cols)
        new_df.loc[:,'id'] = id
        list_of_dfs = list_of_dfs + [new_df]
    inno_raw_df = pd.concat(list_of_dfs).merge(
        geomarkets_df.rename({'longitude':'scan_longitude', 'latitude':'scan_latitude'},axis = 1), on='id')
    inno_raw_df.rename({
        'latitude':'result_latitude',
        'longitude':'result_longitude',
        'geomarketId':'Geomarket'
        }, axis=1, inplace=True)

    return inno_raw_df


def get_innoplexia_cleaned(
    bounds_dict: dict,
    industry_mapping_path: str,
    innoplexia_token: str
    ):
    # TODO: replace with API Call
    # TODO: make asynchronous
    # TODO: check file availability in temporary folder, only create tasks based on missing data

    geomarkets_df = get_available_geomarkets(
        token = innoplexia_token
    )

    innoplexia_raw_df = get_geomarket_data(
        geomarkets_df.id[:100],
        geomarkets_df,
        token = innoplexia_token
    )

    # async call will return a list of response
    # list comprehension to unpack in new list
    # pd.concat to create df
    # dir_path = r'shared_directory\score_attraction\input\temp'
    # files_to_load = os.listdir(dir_path)
    # list_of_dfs = [
    #     pd.read_csv(os.path.join(dir_path,filename)) for filename in files_to_load
    # ]
    # innoplexia_raw_df = pd.concat(list_of_dfs).reset_index(drop=True)
    innoplexia_raw_df = innoplexia_raw_df.loc[
        (innoplexia_raw_df.result_latitude >= bounds_dict['lat_min'])
        & (innoplexia_raw_df.result_latitude <= bounds_dict['lat_max'])
        & (innoplexia_raw_df.result_longitude >= bounds_dict['lon_min'])
        & (innoplexia_raw_df.result_longitude <= bounds_dict['lon_max'])
    #    & (innoplexia_raw_df.Datum == innoplexia_raw_df.Datum.max())
    ].reset_index(drop=True)
    innoplexia_raw_df.loc[:,'scan_result_distance'] = [
        distance for distance
        in compute_search_result_distance(
            innoplexia_raw_df.scan_latitude,
            innoplexia_raw_df.scan_longitude,
            innoplexia_raw_df.result_latitude,
            innoplexia_raw_df.result_longitude
        )]

    innoplexia_raw_df.summary.fillna('unbekannt', inplace=True)
    innoplexia_raw_df.loc[:,'google_business_type'] = [
        get_business_type(get_business_type(summary))
        for summary in innoplexia_raw_df.summary
    ]

    industry_mapping = pd.read_csv(industry_mapping_path,delimiter=';')
    innoplexia_raw_df = innoplexia_raw_df.set_index('google_business_type').join(industry_mapping.set_index('google_business_type'), how='left').reset_index()
    innoplexia_raw_df = innoplexia_raw_df.loc[innoplexia_raw_df.is_commercial == 1]

    return innoplexia_raw_df


def get_business_type(summary_str):
    if summary_str is None:
        return None
    # summary_str = summary_str.replace('Anbieter von ','')
    cutoff = summary_str.find(" in ")
    if cutoff > 0:
        return summary_str[:cutoff]
    cutoff = summary_str.find(", ")
    if cutoff > 0:
        return summary_str[:cutoff]
    cutoff = summary_str.find(" im ")
    if cutoff > 0:
        return summary_str[:cutoff]
    if cutoff == -1:
        return summary_str


def extract_local_businesses(
    raw_df: pd.DataFrame,
    lagepolygone_gdf: gpd.GeoDataFrame,
    save_path: str
    ):
    """
    returns and saves businesses in and around musterstadt
    """
    # drop duplicates by location and summary (business type)
    cols_to_round = ['result_longitude','result_latitude']
    for col in cols_to_round:
        raw_df[col] = raw_df[col].round(5)
    businesses_df = raw_df.copy().drop_duplicates(subset=["result_latitude", "result_longitude", "google_business_type"])
    businesses_df = businesses_df.drop_duplicates(subset=["placeId"])
    # turn into gdf
    local_businesses_gdf = gpd.GeoDataFrame(
        businesses_df,
        geometry=gpd.points_from_xy(x=businesses_df.result_longitude, y=businesses_df.result_latitude),
        crs="EPSG:4326"
        )
    local_businesses_gdf = gpd.sjoin(lagepolygone_gdf[['polygon_id','geometry']], local_businesses_gdf, how='right')
    local_businesses_gdf = local_businesses_gdf[
        ['result_latitude','result_longitude',
        'notice','polygon_id',
        'title','google_business_type',
        'lean_usage_type_name', 'lean_usage_type', 'lean_usage_level_one', 'lean_usage_level_two',
        'numberOfReviews','stars'
        ]].drop_duplicates()
    # local_businesses_gdf.summary.fillna('unbekannt', inplace=True)
    # local_businesses_gdf.loc[:,'google_business_type'] = [
    #     get_business_type(get_business_type(summary))
    #     for summary in local_businesses_gdf.summary
    # ]
    local_businesses_df = local_businesses_gdf[['result_latitude','result_longitude','notice','polygon_id','title','google_business_type',
        'lean_usage_type_name', 'lean_usage_type', 'lean_usage_level_one', 'lean_usage_level_two',
        'numberOfReviews','stars']]
    local_businesses_df = local_businesses_df[[
        'result_latitude','result_longitude',
        'notice',
        'polygon_id',
        'title','google_business_type',
        'lean_usage_type_name',
        'lean_usage_type', 'lean_usage_level_one', 'lean_usage_level_two',
        'numberOfReviews','stars'
        ]]
    # save to csv
    local_businesses_df.to_csv(save_path, index=False)
    # return
    return local_businesses_df


def get_local_businesses(
    innoplexia_raw_df: pd.DataFrame,
    lagepolygone_gdf: gpd.GeoDataFrame,
    availability_dict: dict,
    max_file_age: int
    ) -> pd.DataFrame:
    # TODO: add entry to availability_dict
    availability_sub_dict = availability_dict['local_businesses_google_file']

    # if available according to availability dict, then load
    if availability_sub_dict['available'] and \
    availability_sub_dict['age_in_days'] < max_file_age:
    # load
        local_businesses_df = pd.read_csv(availability_sub_dict['path'])
        log_msg = 'Successfully loaded local businesses'
        logging.info(log_msg)


    if (not availability_sub_dict['available']
        or not availability_sub_dict['age_in_days'] < max_file_age):
        log_msg = 'local businesses google too old or na. extracting them'
        logging.info(log_msg)
        # extract
        local_businesses_df = extract_local_businesses(
            innoplexia_raw_df,
            lagepolygone_gdf,
            availability_sub_dict['path']
        )
        local_businesses_df = local_businesses_df.drop_duplicates(subset=['result_latitude','result_longitude','title'])
    return local_businesses_df
