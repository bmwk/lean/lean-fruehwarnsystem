from general_helpers import logging_ini
from tests.validations import check_all_na_columns, check_not_numeric_columns, check_uniqueness

logger = logging_ini()


def indicator_check(selected_indicator_lists):
    # make sure indicators aggregated by median are part of the overall indicator selection
    all_indicator_set = set(
        selected_indicator_lists["INNOPLEXIA"] +
        selected_indicator_lists["PANADRESS"] +
        selected_indicator_lists["WAL"] +
        selected_indicator_lists["IFH"] +
        selected_indicator_lists["GOOGLE_MAPS"])
    assert set(selected_indicator_lists["BUSINESS_INDICATOR_LIST"]).issubset(all_indicator_set)
    business_indicators = selected_indicator_lists.pop("BUSINESS_INDICATOR_LIST")
    logger.info(str(sum([len(el) for el in selected_indicator_lists.values()])) +
                " indicators to be used for the early warning system from " + str(len(selected_indicator_lists.keys()))
                + " sources.")
    logger.info("The following " + str(len(business_indicators)) + " are collected on the business level " +
                str(business_indicators))
    selected_indicator_lists["BUSINESS_INDICATOR_LIST"] = business_indicators
    # add business indicators again so it won"t be missing later on (I don"t get why, but otherwise apparently the
    # global variable is changed


def check_polygons_with_indicators(polygon_indicator_df, indicator_lists):
    assert "polygon_id" in polygon_indicator_df.columns
    # check data types
    indicator_columns = indicator_lists["BUSINESS_INDICATOR_LIST"] + indicator_lists["AREA_INDICATOR_LIST"]
    check_not_numeric_columns(polygon_indicator_df, indicator_columns)
    # check shape
    assert 50 < len(polygon_indicator_df) < 200  # this could be better but it"s a start
    # make sure there are at least as many columns as business indicators
    assert len(indicator_columns) <= polygon_indicator_df.shape[1]
    check_all_na_columns(polygon_indicator_df)
    check_uniqueness(polygon_indicator_df, ["polygon_id"], "final area-based df ")


def check_businesses_with_indicators(businesses_with_all_indicators, selected_indicator_lists):
    assert {"id", "businessName", "latitude", "longitude", "businessType"}.issubset(
        set(businesses_with_all_indicators.columns))
    # check data types
    business_indicators = selected_indicator_lists["BUSINESS_INDICATOR_LIST"]
    check_not_numeric_columns(businesses_with_all_indicators, business_indicators)
    # check shape
    assert len(businesses_with_all_indicators) > 1000
    # make sure there are at least as many columns as business indicators
    assert len(business_indicators) <= businesses_with_all_indicators.shape[1]
    check_all_na_columns(businesses_with_all_indicators)
    check_uniqueness(businesses_with_all_indicators, ["id"], "final business-based df")


def check_google_ifh_mapping(google_ifh_industry_mapping):
    logger.debug(str(len(google_ifh_industry_mapping)) + " google industries mapped to " +
                 str(len(google_ifh_industry_mapping.ifh_category.unique())) + " ifh categories and " +
                 str(len(google_ifh_industry_mapping.ifh_industry.unique())) + " ifh sub-categories.")
    missing_ifh_categories = google_ifh_industry_mapping.ifh_category.isna().sum()
    missing_ifh_industries = google_ifh_industry_mapping.ifh_industry.isna().sum()
    if missing_ifh_categories > 0:
        logger.warning(str(missing_ifh_industries) + " google maps business types could NOT be mapped to an ifh "
                                                     "category")
    else:
        logger.debug("All google maps business types could be mapped to ifh categories.")
    if missing_ifh_industries > 0:
        logger.info(str(missing_ifh_industries) + " google maps business types could NOT be mapped to an ifh "
                                                  "sub-category")
    else:
        logger.debug("All google maps business types could be mapped to ifh sub-categories.")


def check_ifh_growth_rates(growth_rates_per_ifh_industry, columns_of_interest=None):
    if columns_of_interest is None:
        columns_of_interest = ["CAGR_2015-2019", "CAGR_2015-2020", "CAGR_2019-2020", "online_ratio_2020"]
    industry_number = len(growth_rates_per_ifh_industry)
    relevant_ifh_indicators = growth_rates_per_ifh_industry.loc[:, columns_of_interest]
    missing_value_percentages = round(relevant_ifh_indicators.isna().sum()/industry_number * 100, 1)
    if (missing_value_percentages > 20).any():
        logger.warning("Some IFH indicators are missing for more than 20% of the " + str(industry_number) +
                       " IFH industries:" + str(list(missing_value_percentages[missing_value_percentages > 10])))
