from os.path import basename
from geojson.geometry import Polygon
from pandas import DataFrame

from general_helpers import logging_ini

logger = logging_ini()


def assure_numeric_data_type(input_df, columns_to_be_checked):
    for indicator in columns_to_be_checked:
        try:
            input_df.loc[:, indicator] = input_df.loc[:, indicator].str.replace(",", ".").astype(float)
        except AttributeError:
            pass
    return input_df


def check_polygon_file_formatting(unpacked_polygon, filepath):
    filename = basename(filepath)
    assert {"type", "features"}.issubset(set(unpacked_polygon.keys())), \
        f"Polygon-file does not have 'type' and 'features' keys: {filename}"
    assert unpacked_polygon["type"] == "FeatureCollection", \
        f"Polygon-file does not have type 'FeatureCollection' as they should: {filename}"
    assert isinstance(unpacked_polygon["features"], list), \
        f"Polygon-file features do not contain a list as they should: {filename}"
    assert len(unpacked_polygon["features"]) >= 3, \
        f"Polygon-file features do not contain at least 3 elements: {filename}"
    assert {"type", "properties", "geometry"}.issubset(set(unpacked_polygon["features"][0])), \
        f"Polygon-file features do have 'type', 'properties' and 'geometry' keys as they should: {filename}"
    assert unpacked_polygon["features"][0]["type"] == "Feature", \
        f"Polygon-file features do not have type 'Feature' as they should: {filename}"
    assert isinstance(unpacked_polygon["features"][1]["geometry"], Polygon), \
        f"Polygon-file geometry feature does not have type geojson.geometry.Polygon as it should: {filename}"
    assert len(unpacked_polygon["features"][1]["geometry"]["coordinates"][0][0]) == 2, \
        f"Geometry feature coordinates do not contain lists of lists of two-element-list, as they should: {filename}"


def check_indicator_df(indicator_df, indicator_df_name, id_column="id"):
    """ basic checks that all indicator-dataframes should pass"""
    assert isinstance(indicator_df, DataFrame)
    assert id_column in indicator_df.columns, f"'id'-column is missing in indicator-dataframe {indicator_df_name}"
    assert len(indicator_df[id_column].unique()) == len(indicator_df), \
        f"{id_column}-column is not unique in indicator-dataframe {indicator_df_name}"
    assert not check_all_na_columns(indicator_df)[0], \
        f"There are columns containing only NA-values in indicator-dataframe {indicator_df_name}"
    columns_except_id = list(indicator_df.copy().columns)
    columns_except_id.remove(id_column)
    if indicator_df_name != "google_api":  # exception for google api - this is not pretty but no time for more
        indicator_df = assure_numeric_data_type(indicator_df, columns_except_id)
        non_numeric_column_list = check_not_numeric_columns(indicator_df, columns_except_id)
        assert len(non_numeric_column_list) == 0, \
            f"{indicator_df_name} still contains non-numeric columns which are not the id_columns: " \
            f"{non_numeric_column_list}"
    return indicator_df
    # Todo separate "just info"-columns (as from google api) from other indicator code entirely


def check_all_na_columns(input_df):
    nas_per_column = input_df.isna().sum()
    all_na_columns = list(input_df.columns[nas_per_column == len(input_df)])
    all_na_columns_existing = len(all_na_columns) > 0
    if all_na_columns_existing:
        logger.warning("There are columns in the area-based-indicator-dataframe, that contain ONLY NAs:")
        logger.warning(all_na_columns)
    na_values_per_column_percentage = round((nas_per_column / len(input_df) * 100).sort_values(ascending=False), 1)
    logger.info("Percentage of rows with na values per column with NA-values:\n" +
                str(na_values_per_column_percentage[na_values_per_column_percentage > 0]))
    return all_na_columns_existing, na_values_per_column_percentage


def check_missing_values(input_df, column_to_check, decimal_count=0):
    df_len = len(input_df)
    missing_values_absolute = input_df[column_to_check].isna().sum()
    missing_values_in_percent = round(missing_values_absolute * 100 / df_len, decimal_count)
    return missing_values_absolute, missing_values_in_percent


def check_uniqueness(input_df, columns_to_be_unique, name_of_input_df):
    for col_name in columns_to_be_unique:
        if len(input_df[col_name].unique()) != len(input_df):
            logger.warning("Column " + col_name + " is not unique in " + name_of_input_df + ", but it should")


def check_not_numeric_columns(input_df, columns_to_be_numeric):
    not_numeric_data_types = [
        el for el in list(columns_to_be_numeric) if input_df[el].dtype.kind not in "biufc"]
    if len(not_numeric_data_types):
        logger.error("There are still not numeric indicator columns left, that need to be transformed: " +
                     str(not_numeric_data_types))
        return not_numeric_data_types
    else:
        return []
