from os.path import join, dirname, basename
from configparser import ConfigParser
from pandas import read_csv, set_option, options
import pandas as pd
from datetime import datetime as dt
from os import listdir
import os
from sys import argv
# from structlog import get_logger, configure, make_filtering_bound_logger
import logging
# from pandas_profiling import ProfileReport
from re import findall
from time import time
import hashlib


# def logging_ini(log_level=None, logger_name=None):
#     if logger_name is None:
#         logger_name = basename(argv[0]).split(".")[0]
#     if log_level:
#         """ initialize logging on certain level """
#         if log_level == "debug":
#             log_level = logging.DEBUG
#         elif log_level == "info":
#             log_level = logging.INFO
#         elif log_level == "warning":
#             log_level = logging.WARNING
#         elif log_level == "error":
#             log_level = logging.ERROR
#         configure(wrapper_class=make_filtering_bound_logger(log_level))

#         """
#         logging.basicConfig(
#             format="%(message)s",
#             stream=stdout,
#             level=log_level
#         )
#         """

#     return get_logger(logger_name)


# logger = logging_ini()


def basic_settings(logging_level):
    set_option("display.max_columns", 20)
    set_option("display.width", 300)
    main_logger = logging_ini(log_level=logging_level)
    main_logger.info("Frühwarnsystem starting...")
    options.mode.chained_assignment = None  # this is to mute warnings "trying to set a value on a copy of a slice from
    # a dataframe" that I don"t know how to solve otherwise
    return time(), main_logger


def read_section_element_from_config(config_file_name="config.ini", section="data_directory"):
    """ read first element from section in config-file """
    config_file = join(dirname(__file__), config_file_name)
    parser = ConfigParser()
    parser.read(config_file)
    return parser.items(section)[0][1]


def read_prototype_csv_data(path, filename, sep=";", index_col=0):
    complete_filename = join(path, filename)
    return read_csv(complete_filename, sep=sep, index_col=index_col)


def save_to_gdrive(input_df, save_folder, file_name, data_dir=True, encoding="utf-8-sig", sep=";"):
    """ save results to shared google drive folder """
    file_name_with_date = str(dt.now().date()) + "_" + file_name
    if data_dir:
        data_dir = read_section_element_from_config("config.ini", "data_directory")
        save_dir = join(data_dir, save_folder, file_name_with_date)
    else:
        save_dir = join(save_folder, file_name_with_date)
    input_df.to_csv(save_dir, encoding=encoding, sep=sep)
    logger.info("File saved to: " + str(save_dir))


def save_pandas_profile(input_df, saving_path):
    """ save automatically generated html report of input dataframe to saving_path"""
    today = str(dt.now().date())
    profiling_path = join(saving_path, f"{today}_early_warning_profile.html")
    logger.info("Saving results to html at: " + profiling_path)
    profile = ProfileReport(input_df)
    profile.to_file(output_file=profiling_path)
    return profile


def calculate_business_id(business_df, name_col, latitude_col, longitude_cal, rounding_decimals=3):
    business_df["id"] = business_df.apply(
        lambda x: str(round(x[latitude_col], rounding_decimals)) + "_" + str(round(x[longitude_cal], rounding_decimals))
        + "_" + x[name_col].lower(), axis=1)
    return business_df


def drop_duplicates_with_difference(input_df, subset):
    """ function to drop duplicate rows from input dataframe and print the number of rows dropped """
    input_df_wo_duplicates = input_df.drop_duplicates(subset=subset)
    number_of_duplicates = len(input_df) - len(input_df_wo_duplicates)
    logger.info(str(number_of_duplicates) + " duplicates out of " + str(len(input_df)) + " filtered out based on " +
                str(subset) + " column(s).")
    logger.info("Those are " + str(round(number_of_duplicates / len(input_df) * 100)) + " % of all results.")
    return input_df_wo_duplicates


def get_fresh_files_if_existing(path, max_age_in_days):
    """ checking at given path existence of not too old files and return them as list """
    files_in_api_directory = listdir(path)
    valid_files = []
    today = dt.now().date()
    for file_name in files_in_api_directory:
        try:
            date_string = findall(r"\d{4}-\d{2}-\d{2}", file_name)[0]
            api_request_date = dt.strptime(date_string, "%Y-%m-%d").date()
            file_age_in_days = (today - api_request_date).days
            # check if data is less than max_age_in_days old
            if file_age_in_days < max_age_in_days:
                valid_files.append((file_name, file_age_in_days))
        except IndexError:
            logger.warning("Should file with name: " + file_name + " really be here (should contain date at the " +
                           "beginning of the file name, but it doesn't):\n" + path)
            pass
    return valid_files

def get_api_tokens(
    sources: list, 
    path_to_file: str
    ) -> dict:
    # TODO: replace the code, token available through LeAn solution
    """ loads and tests API tokens """
    logging.info("loading api tokens")
    try:
        keys_df = pd.read_excel(
            os.path.join(path_to_file,'api schluessel einfuegen.xlsx')
            ,header=None,index_col=0)
    except FileNotFoundError as e:
        logging.exception("file missing for API tokens: {e}")
    token_dict = {}
    for source in sources:
        if keys_df.loc[source].iloc[0] is not None:
            token_dict[source] = keys_df.loc[source].iloc[0]
            #TODO: add test call to api to see if connection is working correctly
            logging.info(f"token for {source} available and working")
        else:
            logging.error(f'API Key nicht gefunden für {source}')
    return token_dict


def all_files_available(
        files_to_be_loaded: dict,
        input_path
        ) -> bool:
    files_in_directory = os.listdir(input_path)
    files_available = {
        filename for filename in files_to_be_loaded.values() if (filename in files_in_directory) 
    }
    files_missing = list(set(files_to_be_loaded.values()) - files_available)
    unused_files_in_directory = list(set(files_in_directory) - files_available)
    if len(files_missing) > 0:
        logging.error(f'files missing in input directory: {files_missing}')
    if len(unused_files_in_directory) > 0:
        logging.warning(f'unused files available in input directory: {unused_files_in_directory}')
    return len(files_missing) == 0


def update_availability(path_to_file: str, key: str) -> dict:
    if os.path.exists(path_to_file):
        file_age = int((
            dt.now().timestamp() 
            - os.path.getmtime(path_to_file)
        ) / 86400)
        update_dict = {
            key:{
                'available':True,
                'age_in_days':file_age,
                'path':path_to_file
            }
        }
        log_msg = f'File available. Path: {path_to_file}'
        logging.info(log_msg)
    if not os.path.exists(path_to_file):
        update_dict = {}
        log_msg = f'File not found. Path: {path_to_file}'
        logging.info(log_msg)
    return update_dict


# availability checks
# TODO: Check, ob bare minimum verfügbar ist
# API test calls
# input file lookup
# TODO: ausbauen
## - check percentile availability + freshness
## - check matched availability + freshness
## - check file availability + freshness
## - check city in raw files
def analyse_availability(
    input_path: str, 
    intermediate_path: str,
    files_to_be_loaded_dict: dict,
    test_all=False
) -> dict:
    # construct dict with base values
    availability_dict = {
        'city_zscore_file':{
            'available':False,
            'age_in_days':None,
            'path':os.path.join(intermediate_path,'city_indicators.pkl')
            },
        'city_value_file':{
            'available':False,
            'age_in_days':None,
            'path':os.path.join(intermediate_path,'city_indicator_values.csv')
        },
        'wegweiser_file':{
            'available':False,
            'age_in_days':None,
            'path':os.path.join(input_path,'population_change.csv')
        },
        'polygon_percentile_file':{
            'available':False,
            'age_in_days':None,
            'path':os.path.join(intermediate_path,'polygon_percentile.csv')
        },
        'local_businesses_google_file':{
            'available':False,
            'age_in_days':None,
            'path':os.path.join(intermediate_path,'local_businesses_google.csv')
        },
        'percentile_file_available':'no',
        'percentile_file_freshness':'na',
        'city_included_in_percentile_file':'na',
        'city_included_in_indicator_values_file':'na',
        'all_single_files_available':all_files_available(
            files_to_be_loaded_dict,
            input_path
        )
    }
    files_to_lookup = [
        'city_zscore_file',
        'city_value_file',
        'wegweiser_file',
        'polygon_percentile_file',
        'local_businesses_google_file'
    ]
    if not test_all:
        for file in files_to_lookup:
            availability_dict.update(update_availability(
                availability_dict[file]['path'],
                file
                ))
    return availability_dict


def remove_dot(text):
    if text is None:
        return None
    if type(text) == str:
        if text.find('.') > 0:
            return text[1 + text.find('.'):]
        else:
            print(text)
            return None


def replacer_and_float_caster(text, str_to_replace = 'k.A.'):
    if text == str_to_replace:
        return None
    else:
        return float(text)


def hash_business(
    lat, long, name
    ):
    hash = hashlib.sha256()
    hash.update(str(lat).encode())
    hash.update(str(long).encode())
    hash.update(str(name).encode())
    
    return hash.hexdigest()