import pandas as pd
import logging
import os

def local_rank(
    inno_raw_df: pd.DataFrame
    ):
    try:
        inno_raw_df = inno_raw_df.copy()[[
            'title',
            'result_latitude','result_longitude',
            'scan_latitude','scan_longitude',
            'scan_result_distance',
            'Geomarket',
            'position','google_business_type']].reset_index(drop=True)
    except KeyError as error:
        log_msg = f'Missing columns in innoplexia data: {error}'
        logging.error(log_msg)
    # get best ranks
    best_rank_index = inno_raw_df.groupby(['result_latitude','result_longitude','title','Geomarket']).position.idxmin()
    best_rank_df = inno_raw_df.loc[best_rank_index]
    # get minimum distance between search and result location from all the best ranks
    min_dist_index = best_rank_df.groupby(['result_latitude','result_longitude','title']).scan_result_distance.idxmin()
    local_rank_df = best_rank_df.loc[min_dist_index].rename({'position':'local_search_position'},axis=1)
    return local_rank_df[['result_latitude','result_longitude','title','local_search_position']]



def potential_household_reach(
    inno_raw_df: pd.DataFrame,
    input_path: str
    ) -> pd.DataFrame:
    ctr_ref = pd.read_excel(os.path.join(input_path,'google_rank__click_through_rate.xlsx'))[['Rank','CTR']]
    inno_raw_df = inno_raw_df.merge(ctr_ref,left_on='position',right_on='Rank')
    inno_raw_df.loc[:,'potential_reach__households'] = inno_raw_df.CTR*2.5
    potential_reach_df = inno_raw_df.groupby(['result_latitude','result_longitude','title']).potential_reach__households.sum()
    return potential_reach_df.reset_index()


def get_business_indicators(
    local_businesses_df: pd.DataFrame,
    google_results_raw_df: pd.DataFrame,
    input_path: str
    ):

    local_rank_df = local_rank(
        google_results_raw_df
    )

    potential_reach_df = potential_household_reach(google_results_raw_df, input_path)
    
    all_indicators_df = (
        local_businesses_df
        .merge(local_rank_df, on=['result_latitude','result_longitude','title'], how='left')
        .merge(potential_reach_df, on=['result_latitude','result_longitude','title'], how='left')
    )


    business_indicators_dict = {
    'stars':1,
    'numberOfReviews':1,
    'local_search_position':-1,
    'potential_reach__households':1
}

    grouped_df = all_indicators_df.groupby(['propertyUsers.industryClassification.levelOne'])
    for indicator, direction in business_indicators_dict.items():
        if direction == 1:
            all_indicators_df.loc[:,indicator+'_percentile'] = grouped_df[indicator].rank(pct=True)
        if direction == -1:
            all_indicators_df.loc[:,indicator+'_percentile'] = grouped_df[indicator].rank(pct=True, ascending=False)

    return all_indicators_df
