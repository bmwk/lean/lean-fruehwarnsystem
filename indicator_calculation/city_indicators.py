import logging
import pandas as pd
import json
import requests


def get_population_change(
    max_file_age: int,
    availability_dict: dict
    ) -> pd.DataFrame:
    availability_sub_dict = availability_dict['wegweiser_file']
    if availability_sub_dict['available'] and \
    availability_sub_dict['age_in_days'] < max_file_age:
        population_df = pd.read_csv(availability_sub_dict['path'])
        log_msg = 'population change data loaded from shared dir'
        logging.info(log_msg)
    # if not available or too old, then call api
    if (not availability_sub_dict['available'] 
        or not availability_sub_dict['age_in_days'] < max_file_age):
        log_msg = 'population change data not available or too old. requestion now'
        logging.info(log_msg)
        # params & url
        wegweiser_url = (
            'https://www.wegweiser-kommune.de/data-api/rest/export/'
            + 'bevoelkerungsentwicklung-ueber-die-letzten-5-jahre'
            + '%2Bgemeinden-und-staedte-mit-20000-50000-einwohnern-mit-50000-100000-einwohnern-mit-100000-500000-einwohnern-mit-500000-einwohnern'
            + '%2B2020%2Btabelle.json'
        )
        wegweiser_params = {
            'download':'false',
            'raw':'true'
            }
        # call
        response = requests.get(wegweiser_url, wegweiser_params)

        # unpack response
        nested_data = pd.json_normalize(json.loads(response.text))[['data.regions','data.indicators']]
        staedte_series = pd.Series([
            item.get('name')
            for item
            in nested_data['data.regions'][0]
        ])
        bevoelkerungswachstum_series = pd.Series([
            item[0]
            for item
            in nested_data['data.indicators'][0][0].get('regionYearValues')
        ])
        # turn response into DF
        population_df = pd.DataFrame([staedte_series,bevoelkerungswachstum_series]).T
        population_df.columns = ['stadt','Bevoelkerungswachstum']
        population_df.to_csv(
            availability_sub_dict['path'],
            index = False
        )
        log_msg = 'population change data successfully requested and saved'
        logging.info(log_msg)
    return population_df


def get_city_indicator_values(
    max_file_age: int,
    availability_dict: dict,
    ifh_file_path: str,
    stadt: str
):
    availability_sub_dict = availability_dict['city_value_file']
    # if available according to availability dict, then load
    if availability_sub_dict['available'] and \
        availability_sub_dict['age_in_days'] < max_file_age:
        city_indicator_df = pd.read_csv(availability_sub_dict['path'])
        log_msg = 'Successfully loaded city_indicator_values'
        logging.info(log_msg)   
    # if not available or too old, then compute
    if (not availability_sub_dict['available'] 
        or not availability_sub_dict['age_in_days'] < max_file_age):
        population_df = get_population_change(
            max_file_age,
            availability_dict
        )
        local_population_df = population_df.loc[population_df.stadt.str.contains(stadt)]
        ## get IFH data on city level
        city_indicator_df = pd.read_excel(ifh_file_path)
        ## joins mit guten & best matches
        city_indicator_df.loc[:,'Bevoelkerungswachstum'] = local_population_df.Bevoelkerungswachstum.iloc[0]
        city_indicator_df.to_csv(availability_sub_dict['path'],index=False)
        log_msg = 'city indicator values computed and saved'
        logging.info(log_msg)
    return city_indicator_df


# city indicator scoring foundation (z-score)
def get_city_indicators(
    max_file_age: int,
    availability_dict: dict,
    path_to_city_reference: str, 
    path_to_converter: str,
    city_indicators_list: list,
    ifh_file_path: str,
    stadt: str
    ):

    availability_sub_dict = availability_dict['city_zscore_file']
    # if available according to availability dict, then load
    if availability_sub_dict['available'] and \
    availability_sub_dict['age_in_days'] < max_file_age:
        combined_df = pd.read_pickle(availability_sub_dict['path'])
        log_msg = 'Successfully loaded city_zscore_file'
        logging.info(log_msg)    
    
    # if not available or too old, then compute
    if (not availability_sub_dict['available'] 
        or not availability_sub_dict['age_in_days'] < max_file_age):
        log_msg = 'city_indicator_dict too old or na. computing it'
        logging.info(log_msg)
        values = get_city_indicator_values(
            max_file_age,
            availability_dict,
            ifh_file_path,
            stadt
        )
        references = pd.read_excel(path_to_city_reference)
        filtered_references = references.loc[references.ogk == values.ogk[0]]
        combined_df = pd.concat([values,filtered_references])
        combined_df.set_index('nameStadt', inplace=True)
        for indicator in city_indicators_list:
            combined_df[indicator + '__zscore'] = (
                (combined_df[indicator].iloc[0] - combined_df[indicator]['mean']) 
                /
                combined_df[indicator]['std'] * combined_df[indicator]['direction']
            )
            
        combined_df = combined_df.iloc[0].drop(['codeStadt','Gemeindeschluessel','ogk'])
        combined_df.to_pickle(availability_sub_dict['path']+'zscore')
        combined_df = city_zscore_to_percentile(combined_df, path_to_converter)
        combined_df.to_pickle(availability_sub_dict['path'])
    return combined_df


def city_zscore_to_percentile(city_zscores, reference_file_path):
    city_zscores_df = (round(city_zscores.iloc[int(len(city_zscores)/2):]*4)/4).reset_index()
    city_zscores_df.columns = ['index_old','z']
    converter_df = pd.read_pickle(reference_file_path)
    city_percentiles = pd.merge(city_zscores_df, converter_df, on='z')
    city_percentiles.index = [
        item.replace('zscore','percentile')
        for item in city_percentiles.index_old
    ]
    city_percentiles = city_percentiles.percentile
    city_percentiles.loc[city_percentiles < 0] = 0
    city_percentiles.loc[city_percentiles > 1] = 1
    return pd.concat([
        city_zscores.iloc[:int(len(city_zscores)/2)],  # raw values
        city_percentiles
        ])