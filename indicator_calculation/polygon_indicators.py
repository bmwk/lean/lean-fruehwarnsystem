import geopandas as gpd
import pandas as pd
import logging


def get_polygon_indicators(
    lean_gdf: gpd.GeoDataFrame,
    # only based on lean building units because they overrepresent ground level building units
    # and ground level building units have far more influence over pedestrians' perception of attractiveness 
    availability_dict: dict,
    max_file_age: int,
    min_businesses_per_polygon: int
) -> pd.DataFrame:
    availability_sub_dict = availability_dict['polygon_percentile_file']
    if availability_sub_dict['available'] and \
    availability_sub_dict['age_in_days'] < max_file_age:
        leerstandsquote_df = pd.read_csv(availability_sub_dict['path'])
        log_msg = 'polygon percentile data loaded from shared dir'
        logging.info(log_msg)
    # if not available or too old, then compute
    if (not availability_sub_dict['available'] 
        or not availability_sub_dict['age_in_days'] < max_file_age):
        log_msg = 'polygon percentile data not available or too old. requestion now'
        logging.info(log_msg)
        leerstandsquote_df = pd.DataFrame(lean_gdf.groupby(['polygon_id']).objectIsEmpty.agg(['count','mean']))
        leerstandsquote_df.columns = ['n_of_building_units','leerstandsquote']
        leerstandsquote_df.loc[leerstandsquote_df['n_of_building_units'] < min_businesses_per_polygon,'leerstandsquote'] = pd.NA
        leerstandsquote_df.loc[:,'leerstandsquote__percentile'] = leerstandsquote_df.leerstandsquote.rank(pct=True, ascending=False)
        leerstandsquote_df.to_csv(availability_sub_dict['path'])
    return leerstandsquote_df


def add_polygon_indicators(
    local_businesses_df,
    leerstandsquote_percentile_df
    ):
    local_businesses_df = local_businesses_df.merge(
        leerstandsquote_percentile_df,
        how='left',
        left_on = 'lagepolygon_id',
        right_on = 'polygon_id'
    )  
    return local_businesses_df 