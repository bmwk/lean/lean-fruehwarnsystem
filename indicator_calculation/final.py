import pandas as pd
import numpy as np

scoring_weights_dict = {
    'business_level':0.4,
    'industry_level':0.3,
    'polygon_level':0.1,
    'city_level':0.2
}

scoring_thresholds_dict = {
    'business_level':{
        'low_pressure':0.6,
        'high_pressure':0.3
    },
    'industry_level':{
        'low_pressure':0.6,
        'high_pressure':0.3
    },
    'polygon_level':{
        'low_pressure':0.6,
        'high_pressure':0.3
    },
    'city_level':{
        'low_pressure':0.6,
        'high_pressure':0.3
    }
}

levels_list = [
    'business_level',
    'industry_level',
    'polygon_level',
    'city_level'
]


def create_full_indicator_df(
    business_indicators_df,
    local_businesses_with_industry_indicators,
    leerstandsquote_percentile_df,
    city_indicators_df   
    ):
    # add industry info
    all_indicators_df = business_indicators_df.join(
        local_businesses_with_industry_indicators[[
            'Marktvolumen', 'branchenentwicklung_longterm', 'branchenentwicklung_shortterm',
            'fachhandelsanteil_aktuell', 'fachhandelsanteil_entwicklung',
            'onlineanteil_aktuell', 'onlineanteil_entwicklung','ratio_stationaer_online',
            'Marktvolumen__percentile',
            'branchenentwicklung_longterm__percentile',
            'branchenentwicklung_shortterm__percentile',
            'fachhandelsanteil_aktuell__percentile',
            'fachhandelsanteil_entwicklung__percentile',
            'onlineanteil_aktuell__percentile',
            'onlineanteil_entwicklung__percentile',
            'ratio_stationaer_online__percentile'
        ]]
    )

    # polygon indicators added
    all_indicators_df = all_indicators_df.merge(
        leerstandsquote_percentile_df[['leerstandsquote','leerstandsquote__percentile', 'polygon_id']],
        how='left',
        left_on='lagepolygon_id',
        right_on = 'polygon_id')

    for indicator in city_indicators_df.index:
        all_indicators_df.loc[:,indicator] = city_indicators_df[indicator]

    return all_indicators_df


def score_pressure(
    value,
    thresholds,
    weight
    ):
    score_reduction = 0
    if pd.isna(value):
        score_reduction = score_reduction + weight*50
    if value < thresholds['low_pressure']:
        score_reduction = score_reduction + weight*50
    if value <= thresholds['high_pressure']:
        score_reduction = score_reduction + weight*50
    return score_reduction


def get_classification(score_reduction):
    if 100-score_reduction > 60:
        return 2
    if (100-score_reduction <= 60) & (100-score_reduction > 30):
        return 1
    if 100-score_reduction <= 30:
        return 0


def score_pressures(all_indicators_df):
    for level in levels_list:
        all_indicators_df.loc[:,level+'_score_reduction'] = [
            score_pressure(mean_value, scoring_thresholds_dict[level], scoring_weights_dict[level])
            for mean_value in all_indicators_df[level+'_mean']
        ]
    
    all_indicators_df.loc[:,'pressure_classification'] = [
        get_classification(reduction)
        for reduction
        in all_indicators_df.business_level_score_reduction
            + all_indicators_df.industry_level_score_reduction
            + all_indicators_df.polygon_level_score_reduction
            + all_indicators_df.city_level_score_reduction
    ]
    return all_indicators_df

# check missing ids
def add_missing_business_location_areas(
    lagepolygone_df: pd.DataFrame,
    business_location_area_scoring: pd.DataFrame
):
    lagepolygone_df.rename({'polygon_id':'business_location_area_id'},axis=1, inplace=True)
    business_location_area_scoring.rename({'polygon_id':'business_location_area_id'},axis=1, inplace=True)
    ids_without_score = set(lagepolygone_df.business_location_area_id).difference(set(business_location_area_scoring.business_location_area_id))
    ids_without_score_df = lagepolygone_df.query(f'business_location_area_id in {tuple(ids_without_score)}')[['business_location_area_id']]
    return pd.concat([ids_without_score_df, business_location_area_scoring])