import pandas as pd
from general_helpers import remove_dot, replacer_and_float_caster


# TODO: replace hard coded solution with dynamic option
industry_indicator_list = [
        'Marktvolumen',
        'branchenentwicklung_longterm',
        'branchenentwicklung_shortterm',
        'fachhandelsanteil_aktuell',
        'fachhandelsanteil_entwicklung',
        'onlineanteil_aktuell',
        'onlineanteil_entwicklung',
        'ratio_stationaer_online'
    ]
    
def get_industry_data(
    path: str
    ) -> pd.DataFrame:
    # TODO: replace hard coded rows with dynamic solution
    industry_df = pd.read_excel(path,header=1).drop([0,1],axis=0)[:112]
    industry_df.columns = [
        'Status',
        'Nutzungsart',
        'NutzungsartText',
        'Differenzierungsebene1',
        'Differenzierungsebene1Text',
        'Differenzierungsebene2',
        'Differenzierungsebene2Text',
        'WZ',
        'bsp1','bsp2',
        'leer'
    ] + industry_indicator_list
    industry_df.Differenzierungsebene1 = [
        remove_dot(item) for item in industry_df.Differenzierungsebene1
    ]
    industry_df.Differenzierungsebene2 = [
        remove_dot(item) for item in industry_df.Differenzierungsebene2#.fillna('') if item.find('.')>0
    ]
    industry_df.Differenzierungsebene2 = [
        remove_dot(item) for item in industry_df.Differenzierungsebene2#.fillna('') if item.find('.')>0
    ]

    industry_df.Nutzungsart = industry_df.Nutzungsart.fillna(1).astype(int)
    industry_df.Differenzierungsebene1 = industry_df.Differenzierungsebene1.fillna(1).astype(int)
    industry_df.Differenzierungsebene2 = industry_df.Differenzierungsebene2.fillna(1).astype(int)

    for indicator in industry_indicator_list:
        industry_df[indicator] = [
            replacer_and_float_caster(cell) for cell in industry_df[indicator]
        ]
        industry_df[indicator] = industry_df[indicator].fillna(industry_df[indicator]).astype(float)
        
    return industry_df[[
        'Nutzungsart',
        'Differenzierungsebene1',
        'Differenzierungsebene2'
        ] + industry_indicator_list
    ]


industry_indicator_wirkrichtung_dict = {
    'Marktvolumen':0,
    'branchenentwicklung_longterm':1,
    'branchenentwicklung_shortterm':1,
    'fachhandelsanteil_aktuell':0,
    'fachhandelsanteil_entwicklung':1,
    'onlineanteil_aktuell':0,
    'onlineanteil_entwicklung':-1,
    'ratio_stationaer_online':1
}


def get_industry_based_percentiles(local_businesses_df: pd.DataFrame):
    for indicator, direction in industry_indicator_wirkrichtung_dict.items():
        local_businesses_df[indicator] = local_businesses_df[indicator] * direction
    industry_percentile_df = local_businesses_df[
        [key for key, value in industry_indicator_wirkrichtung_dict.items()]
    ].rank(pct=True)
    return industry_percentile_df