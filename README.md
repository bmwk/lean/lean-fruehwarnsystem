This is an MVP-project to aggregate data on single german cities from different sources and calculate different indicators. The indicators are used to evaluate the effect local businesses have on the perceived attractiveness of the surrounding area.

## Result
The end result of the script is a number of tables pushed to a (maria)-db.

## Installation
Make sure you are working in a virtual environment. If you don"t know how to do that look it up. We recommend working with conda:
https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html
To setup the environment and all dependencies, run 
`conda env create -f environment.yml`

If you added new packages make sure you also add them to the environment-file. 

## Prerequisites 
- **city-specific config**: Most of the city-specific config will be provided in the shared_directory. The data is up to date as of september 2022.
There needs to be a table available in the database with the API token for the innoplexia API endpoints.
- **general config**: To successfully run the script you need to have all the necessary data files in a folder whose name you can define in the `config.ini` file. Therefore, you must first create this file. The easiest way is to copy the `config.example` found in this repository, then rename it to config.ini and then fill in your own values. 
The results will also be saved as csv-Files in a "result" subfolder in the folder named under _data_directory_ in the config-File.


## General project structure
#### main file
`__main__.py` can be run to calculate scores for polygons and business per city
#### shared_directory
Necessary input data needs to be stored here.
#### data_prep
Preparing data accessed via API endpoints.
#### indicator_calculation
Here the cleaned data is turned into finished indicators. Validation- and probability-checks should be done here. Here the finished indicators are combined and aggregated as well.
#### geo_transformations
All code related to transforming data related to geographic shapes.
#### tests
Tests, plausibility checks and validation functions.
**# Todo**: Further tests and plausibility checks should be added

#### Explorations & Analysis
Folders containing scripts used just for exploratory data analysis.
Those can be checked and re-used but don't necessarily need to be kept-up to date and should not be used in any of the "prodicutive" code (in the main file.)

## Code style-guide
...can be found here: https://google.github.io/styleguide/pyguide.html

## Work in Progress
All the essential parts should be there, for whatever is missing search the code for `# Todo`

## Run
### Get early warning scores
- run `__main__.py`
### Tests
- Can be executed by running `python -m pytest` in the console
- Further, tests can be added by creating a python-File starting with "test_" in the folder _tests_

