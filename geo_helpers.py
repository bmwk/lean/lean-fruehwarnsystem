import geopandas as gpd
import pandas as pd
from db_connect import db_manager
import json
import shapely


def create_list_of_tuples(list_of_dicts):
    return [
        (item['x'],item['y'])
        for item in list_of_dicts
    ]


def get_lagepolygone(
    schema_name,
    table_name='business_location_area', 
    ):
    area_df = db_manager.read_lagepolygone(schema_name, table_name)

    intermediate_df = pd.concat([
        pd.json_normalize(json.loads(record))
        for record in area_df.json_data
    ]).reset_index(drop=True)

    intermediate_df.loc[:,'list_of_points_dict'] = [
        item[0]['points']
        for item in intermediate_df['geolocationPolygon.polygon.rings']
    ]

    intermediate_df.loc[:,'list_of_tuples'] = [
        create_list_of_tuples(record)
        for record in intermediate_df.loc[:,'list_of_points_dict']
    ]

    intermediate_df.loc[:,'geometry'] = [
        shapely.geometry.LineString(record)
        for record in intermediate_df.loc[:,'list_of_tuples'] 
    ]

    # include business_location_area_id here because it's missing in the json
    intermediate_df.loc[:,'business_location_area_id'] = area_df.business_location_area_id
    intermediate_df.rename({
        'business_location_area_id':'polygon_id'
    }, axis=1, inplace=True)
    intermediate_df = intermediate_df[['polygon_id','locationCategory','geometry']].sort_values('locationCategory').reset_index(drop=True)
    lagepolygone_gdf = gpd.GeoDataFrame(data=intermediate_df, geometry = intermediate_df.geometry, crs = 'epsg:4326')
    lagepolygone_gdf.geometry = [
        shapely.geometry.Polygon(geo) for geo in lagepolygone_gdf.geometry
    ]
    return lagepolygone_gdf


def create_lean_geodf(lean_df: pd.DataFrame):
    lean_gdf = gpd.GeoDataFrame(lean_df, geometry = gpd.points_from_xy(
        lean_df['geolocationPoint.point.longitude'],
        lean_df['geolocationPoint.point.latitude']))
    lean_gdf.set_crs('epsg:4326',inplace=True)
    lean_gdf.set_index('building_unit_id')
    return lean_gdf


from shapely.geometry import MultiPolygon
def get_inner_city_bounds(lagepolygone_gdf):
    inner_city_mp = MultiPolygon([
        polygon for polygon in lagepolygone_gdf.geometry
    ])
    bounds = inner_city_mp.bounds
    bounds_dict = {
        'lon_min':bounds[0],
        'lat_min':bounds[1],
        'lon_max':bounds[2],
        'lat_max':bounds[3]
    }
    return bounds_dict