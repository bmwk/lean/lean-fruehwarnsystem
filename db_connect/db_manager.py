from array import array
import logging
from typing import List
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine
import pandas as pd
import json
import logging

# ======= edited =======
import os
# ======================


def start_engine() -> Engine:

    # ======= edited =======
    connection_string = os.environ.get('CONNECTIONSTRING')
    # connection_string =
    # ======================


    logging.info(f"Establishing connection with database.")
    return create_engine(connection_string, echo = False)


def get_api_token(
    table_name: str,
    schema_name: str,
    service: str
    ):
    with start_engine().connect() as conn:
        df = pd.read_sql_table(table_name=table_name, schema=schema_name, con=conn)
        innoplexia_token = df.loc[df.service == service,'token'][0]

        return innoplexia_token


def read_lagepolygone(
    schema_name: str,
    table_name: str
    ):
    with start_engine().connect() as conn:
        df = pd.read_sql_table(table_name=table_name, schema=schema_name, con=conn)
        return df



def read_table(table_name: str = "building_unit", schema_name: str = "musterstadt_ews") -> pd.DataFrame:
    # Establish connection with db
    conn = start_engine().connect()
    # Query db for data and load to df
    df = pd.read_sql_table(table_name=table_name, schema=schema_name, con=conn)
    # Check if db_table has json_data col
    assert "json_data" in df.columns, "Could not find column named 'json_data'."
    # Explode json format
    df_js = pd.json_normalize(df["json_data"].apply(json.loads))
    df_js = df_js.rename(columns={"id": f"building_unit_id"})

    # TODO: refactor?
    if table_name == "building_unit" or table_name == 'building_unit_immovativ':
        # Transform from building groupings to business units
        df_js_exploded = df_js.explode(column="propertyUsers")
        tmp = pd.json_normalize(df_js_exploded["propertyUsers"]).set_index(df_js_exploded.index)
        tmp.columns = ["propertyUsers." + c for c in tmp.columns]
        df_js = pd.concat([df_js_exploded, tmp], axis=1)

    # Join with rest of data and filter
    df_lean = df.merge(df_js, how='left', on=f'building_unit_id')
    col_filter = [
        "building_unit_id", "objectIsEmpty", "objectBecomesEmpty", "propertyUsers.propertyUserStatus",
        "geolocationPoint.point.latitude", "geolocationPoint.point.longitude", "propertyUsers.person.name",
        "propertyUsers.industryClassification.name", "propertyUsers.industryClassification.levelOne",
        "propertyUsers.industryClassification.levelTwo", "propertyUsers.industryClassification.levelThree"
    ]

    df_lean = df_lean[col_filter].copy()
    return df_lean


def create_table(
    df: pd.DataFrame,
    columns: list,
    table_name: str,
    schema_name: str,
    primary_key: str
    ):
    '''Creates table in database specified in environment variable.
    If you want to keep the index, make sure it's a column.
    The regular index gets dropped.'''
    clean_df = df[columns]
    with start_engine().connect() as conn:
        clean_df.to_sql(table_name, con=conn,if_exists="replace", schema=schema_name, index=False
            #,dtype = {'`geometry`':Geometry('POINT',)}
        )
        conn.execute(f"Alter table {table_name} add primary key (`{primary_key}`);")


# alteration dict is used to clean up automated table definitions
alteration_dict = {
    'building_units_scoring':[
        'building_unit_id',
        'taod_building_unit_id',
        'business_location_area_id',
        'business_level_score_reduction',
        'industry_level_score_reduction',
        'business_location_area_level_score_reduction',
        'city_level_score_reduction',
        'pressure_classification'
        ],
    'enriched_building_units':[
        'building_unit_id',
        'taod_building_unit_id',
        'business_location_area_id',
        'propertyUsers_industryClassification_levelOne',
        'propertyUsers_industryClassification_levelTwo',
        'propertyUsers_industryClassification_levelThree'
    ]
    ,
    'business_location_area_scoring':['business_location_area_id'],
}


def alter_tables():
    with start_engine().connect() as conn:
        for table_name, col_list in alteration_dict.items():
            for col_name in col_list:
                conn.execute(f'ALTER TABLE {table_name} MODIFY {col_name} INTEGER(10) unsigned')
        add_point_stmt = '''
            ALTER TABLE enriched_building_units
                ADD geolocation_point POINT GENERATED ALWAYS AS (ST_GeomFromText(geometry, 4326)) COMMENT "(DC2Type:point)"
        '''
        conn.execute(add_point_stmt)

# def get_delta_load_query(df: pd.DataFrame, table: str) -> str:
#     into_fields = ", ".join([c for c in df.columns])
#     t1_fields = ", ".join(["t1."+c for c in df.columns])

#     delta_load_query = f"""INSERT INTO core.{table} ({into_fields})
#     SELECT {t1_fields}
#     FROM staging.{table} t1
#     LEFT JOIN core.{table} t2
#     ON t1.PRIME_KEY = t2.PRIME_KEY
#     WHERE t2.PRIME_KEY is NULL"""

#     return delta_load_query
